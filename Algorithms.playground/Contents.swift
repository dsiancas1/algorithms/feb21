public class TreeNode {
    public var val: Int
    public var left: TreeNode?
    public var right: TreeNode?
    public init() { self.val = 0; self.left = nil; self.right = nil; }
    public init(_ val: Int) { self.val = val; self.left = nil; self.right = nil; }
    public init(_ val: Int, _ left: TreeNode?, _ right: TreeNode?) {
        self.val = val
        self.left = left
        self.right = right
    }
}


class Solution {
    func maxProductPath(_ grid: [[Int]]) -> Int {
        let n = grid.count
        let m = grid[0].count

        // (min, max)
        var dp: [[(Int, Int)]] = Array(repeating: Array(repeating: (0, 0), count: m), count: n)

        for i in 0..<n {
            for j in 0..<m {
                if i == 0, j == 0 {
                    dp[0][0] = (grid[0][0], grid[0][0])
                } else if i == 0 {
                    let p = dp[0][j-1].0 * grid[0][j]
                    dp[0][j] = (p, p)
                } else if j == 0 {
                    let p = dp[i-1][0].0 * grid[i][0]
                    dp[i][0] = (p, p)
                } else {
                    var minv = min(dp[i-1][j].0, dp[i][j-1].0) * grid[i][j]
                    var maxv = max(dp[i-1][j].1, dp[i][j-1].1) * grid[i][j]
                    if minv > maxv { swap(&minv, &maxv) }
                    dp[i][j] = (minv, maxv)
                }
            }
        }
        
        if dp[n-1][m-1].1 < 0 { return -1 }
        return dp[n-1][m-1].1 % (Int(1e9 + 7))
    }
    
    func countServers(_ grid: [[Int]]) -> Int {
        let n = grid.count
        var total = 0

        let rows = grid.map { $0.reduce(0, +) }
        
        var cols: [Int:Int] = [:]
        
        for (i, v) in rows.enumerated() {
            total += v
            if v == 1 {
                guard let cl = grid[i].firstIndex(of: v) else { return 0 }
                if let n = cols[cl] {
                    if n == 1 {
                        total -= 1
                    }
                } else {
                    var sum = 0
                    for j in 0..<n {
                        sum += grid[j][cl]
                    }
                    cols[cl] = sum
                    if sum == 1 {
                        total -= 1
                    }
                }
            }
        }
        
        return total
    }
    
    func hammingWeight(_ n: Int) -> Int {
        var sum = 0, n = n
        while n != 0 {
            sum += 1
            n &= (n - 1)
        }
        return sum
    }
    
    func trimBST(_ root: TreeNode?, _ low: Int, _ high: Int) -> TreeNode? {
        guard let nRoot = root else { return nil }
            
        if nRoot.val < low {
            return trimBST(nRoot.right, low, high)
        } else if nRoot.val > high {
            return trimBST(nRoot.left, low, high)
        }
            
        nRoot.left = trimBST(nRoot.left, low, high)
        nRoot.right = trimBST(nRoot.right, low, high)
        
        return nRoot
    }
}

let a = Solution()
//a.maxProductPath([[-1,-2,-3],[-2,-3,-3],[-3,-3,-2]])
//a.countServers([[1,1,0,0],[0,0,1,0],[0,0,1,0],[0,0,0,1]])

